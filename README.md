# NachOS assignments: CPU scheduling


## Goal

1. Implement a **multilevel feadback queue** scheduler with aging mechanism as described below:
    - There are 3 levels of queues: L1, L2 and L3. L1 is the highest level queue, and L3 is the lowest level queue.
    - All processes must have a valid **scheduling priority between 0 to 149**.Higher value means higher prority. So 149 is the highest priority, and 0 is the lowest priority.
    - A process with priority between **0 - 49** is in **L3** queue, priority between **50 - 99** is in **L2** queue, and priority between **100 - 149** is in **L1** queue.
    - **L1** queue uses **preemptive SJF** (shortest job first) scheduling algorithm. If current thread has the lowest approximate burst time, it should not be preempted by the threads in the ready queue. The burst time (job execution time) is approximated using the equation:
    $$t_i=0.5*T+0.5*t_{i-1}, i>0, t_0=0$$ 
    Update burst time when state becomes waiting state, stop updating when state becomes ready state, and resume accumulating when state moves back to running state.
    - **L2** queue uses a **non-preemptive priority** scheduling algorithm. A thread in L2 queue won’t preempt other threads in L2 queue; however, it will preempt thread in L3 queue.
    - **L3** queue uses a **round-robin** scheduling algorithm with time quantum **100 ticks** (you should select a thread to run once 100 ticks elapsed). If two threads enter the L3 queue with the same priority, either one of them can execute first.
    - An **aging mechanism** must be implemented, so that the priority of a process is **increased by 10** after waiting for more than **1500 ticks** (Note: The operations of preemption and priority updating can be delayed until the **next timer alarm** interval).
2. Add a command line argument `-ep` for nachos to initialize priority of process. E.g., the command below will launch 2 processes: `test1` with initial priority `40`, and `test2` with initial priority `80`.
    ```
    $ ../build.linux/nachos -ep test1 40 -ep test2 80
    ```
3. Add a debugging flag `z` and use the `DEBUG('z', expr)` macro (defined in `debug.h`) to print following messages. Replace `{...}` to the corresponding value.
    - Whenever a process is inserted into a queue:  
    `[A] Tick [{current total tick}]: Thread [{thread ID}] is inserted into queue L[{queue level}]`
    - Whenever a process is removed from a queue:  
    `[B] Tick [{current total tick}]: Thread [{thread ID}] is removed from queue L[{queue level}]`
    - Whenever a process changes its scheduling priority:  
    `[C] Tick [{current total tick}]: Thread [{thread ID}] changes its priority from [{old value}] to [{new value}]`
    - Whenever a process updates its approximate burst time:  
    `[D] Tick [{current total tick}]: Thread [{thread ID}] update approximate burst time, from: [{ti-1}], add [{T}], to [{ti}]`
    - Whenever a context switch occurs:  
    `[E] Tick [{current total tick}]: Thread [{new thread ID}] is now selected for execution, thread [{prev thread ID}] is replaced, and it has executed [{accumulated ticks}] ticks`


**Please refer to the [report](./MP3_report.pdf) for more details.**

## Appendix: What is NachOS?

> *NachOS* is instructional software for teaching undergraduate, and potentially graduate, level operating systems courses.  
> Website: https://homes.cs.washington.edu/~tom/nachos/
