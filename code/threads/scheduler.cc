// scheduler.cc 
//	Routines to choose the next thread to run, and to dispatch to
//	that thread.
//
// 	These routines assume that interrupts are already disabled.
//	If interrupts are disabled, we can assume mutual exclusion
//	(since we are on a uniprocessor).
//
// 	NOTE: We can't use Locks to provide mutual exclusion here, since
// 	if we needed to wait for a lock, and the lock was busy, we would 
//	end up calling FindNextToRun(), and that would put us in an 
//	infinite loop.
//
// 	Very simple implementation -- no priorities, straight FIFO.
//	Might need to be improved in later assignments.
//
// Copyright (c) 1992-1996 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "debug.h"
#include "scheduler.h"
#include "main.h"

/* add by liuchinlin @ mp3 */
int threadCompare_remainingCpuBurstTime( Thread *x, Thread *y) { // sort from smallest to largest
  if (x->getRemainingCpuBurstTime() < y->getRemainingCpuBurstTime())
      return -1;
  else if (x->getRemainingCpuBurstTime() > y->getRemainingCpuBurstTime())
      return 1;
  else 
      return y->getPriority() - x->getPriority(); // if the same, sort by priority value
}
int threadCompare_priority(Thread *x, Thread *y) { // sort from largest to smallest
    return y->getPriority() - x->getPriority();
}
/* /add by liuchinlin @ mp3 */
//----------------------------------------------------------------------
// Scheduler::Scheduler
// 	Initialize the list of ready but not running threads.
//	Initially, no ready threads.
//----------------------------------------------------------------------

Scheduler::Scheduler()
{ 
    /* /add by liuchinlin @ mp3 */
    // readyList = new List<Thread *>; 
    readyList_L1 = new SortedList<Thread *>(threadCompare_remainingCpuBurstTime); 
    readyList_L2 = new SortedList<Thread *>(threadCompare_priority); 
    readyList_L3 = new List<Thread *>; 
    /* /add by liuchinlin @ mp3 */

    toBeDestroyed = NULL;
} 

//----------------------------------------------------------------------
// Scheduler::~Scheduler
// 	De-allocate the list of ready threads.
//----------------------------------------------------------------------

Scheduler::~Scheduler()
{ 
    // delete readyList; 
    delete readyList_L1; 
    delete readyList_L2; 
    delete readyList_L3; 
} 

//----------------------------------------------------------------------
// Scheduler::ReadyToRun
// 	Mark a thread as ready, but not running.
//	Put it on the ready list, for later scheduling onto the CPU.
//
//	"thread" is the thread to be put on the ready list.
//----------------------------------------------------------------------

void
Scheduler::ReadyToRun (Thread *thread)
{
    ASSERT(kernel->interrupt->getLevel() == IntOff);

    /* modify by liuchinlin @ mp3 */
    // DEBUG(dbgThread, "Putting thread on ready list: " << thread->getName());
	//cout << "Putting thread on ready list: " << thread->getName() << endl ;
    thread->setStatus(READY);

    thread->startAgingCounter();

    // readyList->Append(thread);
    if (thread->getPriority() >= 100) {
        DEBUG(dbgThread, "Tick [" << kernel->stats->totalTicks << "]: Putting thread on ready list L1: " << thread->getName());
        DEBUG(dbgCpuScheduling, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[1]");
        readyList_L1->Insert(thread);
    }
    else if (thread->getPriority() >= 50) {
        DEBUG(dbgThread, "Tick [" << kernel->stats->totalTicks << "]: Putting thread on ready list L2: " << thread->getName());
        DEBUG(dbgCpuScheduling, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[2]");
        readyList_L2->Insert(thread);
    }
    else { 
        DEBUG(dbgThread, "Tick [" << kernel->stats->totalTicks << "]: Putting thread on ready list L3: " << thread->getName());
        DEBUG(dbgCpuScheduling, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[3]");
        readyList_L3->Append(thread);
    }
    /* /modify by liuchinlin @ mp3 */
}

//----------------------------------------------------------------------
// Scheduler::FindNextToRun
// 	Return the next thread to be scheduled onto the CPU.
//	If there are no ready threads, return NULL.
// Side effect:
//	Thread is removed from the ready list.
//----------------------------------------------------------------------

Thread *
/* modify by liuchinlin @ mp3 */
// Scheduler::FindNextToRun ()
Scheduler::FindNextToRun (bool nowIsBlocked)
/* /modify by liuchinlin @ mp3 */
{
    ASSERT(kernel->interrupt->getLevel() == IntOff);

    /* modify by liuchinlin @ mp3 */

    Aging();

    // if (readyList->IsEmpty()) {
	//     return NULL;
    // } else {
    //     return readyList->RemoveFront();
    // }
    Thread *returnThread = NULL;
    if (nowIsBlocked) { // currentThread is now blocked
        if (!(readyList_L1->IsEmpty()))
            returnThread = readyList_L1->RemoveFront();
        else if (!(readyList_L2->IsEmpty()))
            returnThread = readyList_L2->RemoveFront();
        else if (!(readyList_L3->IsEmpty()))
            returnThread = readyList_L3->RemoveFront();
        else // all readyLists are empty
            returnThread = NULL;
    }
    else { // we could let currentThread continue running (return NULL), if appropriate
        Thread *currentThread = kernel->currentThread;
        if (currentThread->getPriority() >= 100) { // currentThread is an L1 thread
            if (!(readyList_L1->IsEmpty())){
                Thread *candidate = readyList_L1->Front();
                if (candidate->getRemainingCpuBurstTime() < currentThread->getRemainingCpuBurstTime())
                    returnThread = readyList_L1->RemoveFront();
            }
        } 
        else if (currentThread->getPriority() >= 50) { // currentThread is an L2 thread
            if (!(readyList_L1->IsEmpty()))
                returnThread = readyList_L1->RemoveFront();
        } 
        else { // currentThread is an L3 thread
            if (!(readyList_L1->IsEmpty()))
                returnThread = readyList_L1->RemoveFront();
            else if (!(readyList_L2->IsEmpty()))
                returnThread = readyList_L2->RemoveFront();
            else if (!(readyList_L3->IsEmpty())) // RR
                returnThread = readyList_L3->RemoveFront();
            else // all readyLists are empty
                returnThread = NULL;
        }
    }
    
    if (returnThread != NULL) {
        returnThread->stopAgingCounter();


        if (returnThread->getPriority() >= 100){
            DEBUG(dbgCpuScheduling, "[B] Tick [" << kernel->stats->totalTicks << "]: Thread [" << returnThread->getID() << "] is removed from queue L[1]");
        }
        else if (returnThread->getPriority() >= 50) {
            DEBUG(dbgCpuScheduling, "[B] Tick [" << kernel->stats->totalTicks << "]: Thread [" << returnThread->getID() << "] is removed from queue L[2]");
        }
        else {
            DEBUG(dbgCpuScheduling, "[B] Tick [" << kernel->stats->totalTicks << "]: Thread [" << returnThread->getID() << "] is removed from queue L[3]");
        }
    }

    return returnThread;
    /* /modify by liuchinlin @ mp3 */
}

/* add by liuchinlin @ mp3 */
void InsertToList(Thread *thread, int from, SortedList<Thread*> *L1, SortedList<Thread*> *L2, List<Thread*> *L3) {
    if (thread->getPriority() >= 100) {
        L1->Insert(thread);
        if (from != 1) {
            DEBUG(dbgCpuScheduling, "[B] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is removed from queue L[" << from << "]");
            DEBUG(dbgCpuScheduling, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[1]");
        }

    }
    else if (thread->getPriority() >= 50) {
        L2->Insert(thread);
        if (from != 2) {
            DEBUG(dbgCpuScheduling, "[B] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is removed from queue L[" << from << "]");
            DEBUG(dbgCpuScheduling, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[2]");
        }
    }
    else {
        L3->Append(thread);
        if (from != 3) {
            DEBUG(dbgCpuScheduling, "[B] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is removed from queue L[" << from << "]");
            DEBUG(dbgCpuScheduling, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[3]");
        }
    }
}

void updateAgingCounter(Thread *thread){
    thread->stopAgingCounter();
    thread->startAgingCounter();
}

void
Scheduler::Aging(void) {

    SortedList<Thread*> *L1_tmp = new SortedList<Thread*>(threadCompare_remainingCpuBurstTime);
    SortedList<Thread*> *L2_tmp = new SortedList<Thread*>(threadCompare_priority);
    List<Thread*> *L3_tmp = new List<Thread*>;
    
    readyList_L1->Apply(updateAgingCounter);
    readyList_L2->Apply(updateAgingCounter);
    readyList_L3->Apply(updateAgingCounter);

    Thread *thread;
    while (!(readyList_L1->IsEmpty())) {
        thread = readyList_L1->RemoveFront();
        thread->increasePriority();
        InsertToList(thread, 1, L1_tmp, L2_tmp, L3_tmp);
    }
    while (!(readyList_L2->IsEmpty())) {
        thread = readyList_L2->RemoveFront();
        thread->increasePriority();
        InsertToList(thread, 2, L1_tmp, L2_tmp, L3_tmp);
    }
    while (!(readyList_L3->IsEmpty())) {
        thread = readyList_L3->RemoveFront();
        thread->increasePriority();
        InsertToList(thread, 3, L1_tmp, L2_tmp, L3_tmp);
    }

    delete readyList_L1;
    delete readyList_L2;
    delete readyList_L3;
    readyList_L1 = L1_tmp;
    readyList_L2 = L2_tmp;
    readyList_L3 = L3_tmp;
}
/* /add by liuchinlin @ mp3 */

//----------------------------------------------------------------------
// Scheduler::Run
// 	Dispatch the CPU to nextThread.  Save the state of the old thread,
//	and load the state of the new thread, by calling the machine
//	dependent context switch routine, SWITCH.
//
//      Note: we assume the state of the previously running thread has
//	already been changed from running to blocked or ready (depending).
// Side effect:
//	The global variable kernel->currentThread becomes nextThread.
//
//	"nextThread" is the thread to be put into the CPU.
//	"finishing" is set if the current thread is to be deleted
//		once we're no longer running on its stack
//		(when the next thread starts running)
//----------------------------------------------------------------------

void
Scheduler::Run (Thread *nextThread, bool finishing)
{
    Thread *oldThread = kernel->currentThread;
    
    ASSERT(kernel->interrupt->getLevel() == IntOff);

    if (finishing) {	// mark that we need to delete current thread
         ASSERT(toBeDestroyed == NULL);
	 toBeDestroyed = oldThread;
    }
    
    if (oldThread->space != NULL) {	// if this thread is a user program,
        oldThread->SaveUserState(); 	// save the user's CPU registers
	oldThread->space->SaveState();
    }
    
    oldThread->CheckOverflow();		    // check if the old thread
					    // had an undetected stack overflow

    kernel->currentThread = nextThread;  // switch to the next thread
    nextThread->setStatus(RUNNING);      // nextThread is now running
    
    /* add by liuchinlin @ mp3 */
    kernel->currentThread->startTimer();

    DEBUG(dbgCpuScheduling, "[E] Tick [" << kernel->stats->totalTicks << "]: Thread [" << kernel->currentThread->getID() << "] is now selected for execution, thread [" << oldThread->getID() << "] is replaced, and it has executed [" << oldThread->getUsedCpuBurstTime() << "] ticks");
    /* /add by liuchinlin @ mp3 */

    DEBUG(dbgThread, "Tick [" << kernel->stats->totalTicks << "]: Switching from: " << oldThread->getName() << " to: " << nextThread->getName());
    
    // This is a machine-dependent assembly language routine defined 
    // in switch.s.  You may have to think
    // a bit to figure out what happens after this, both from the point
    // of view of the thread and from the perspective of the "outside world".

    SWITCH(oldThread, nextThread);

    // we're back, running oldThread
      
    // interrupts are off when we return from switch!
    ASSERT(kernel->interrupt->getLevel() == IntOff);

    DEBUG(dbgThread, "Tick [" << kernel->stats->totalTicks << "]: Now in thread: " << oldThread->getName());

    CheckToBeDestroyed();		// check if thread we were running
					// before this one has finished
					// and needs to be cleaned up
    
    if (oldThread->space != NULL) {	    // if there is an address space
        oldThread->RestoreUserState();     // to restore, do it.
	oldThread->space->RestoreState();
    }
}

//----------------------------------------------------------------------
// Scheduler::CheckToBeDestroyed
// 	If the old thread gave up the processor because it was finishing,
// 	we need to delete its carcass.  Note we cannot delete the thread
// 	before now (for example, in Thread::Finish()), because up to this
// 	point, we were still running on the old thread's stack!
//----------------------------------------------------------------------

void
Scheduler::CheckToBeDestroyed()
{
    if (toBeDestroyed != NULL) {
        delete toBeDestroyed;
	toBeDestroyed = NULL;
    }
}
 
//----------------------------------------------------------------------
// Scheduler::Print
// 	Print the scheduler state -- in other words, the contents of
//	the ready list.  For debugging.
//----------------------------------------------------------------------
void
Scheduler::Print()
{
    /* modify by liuchinlin @ mp3 */
    cout << "Ready list L1 contents:\n";
    readyList_L1->Apply(ThreadPrint);
    cout << "Ready list L2 contents:\n";
    readyList_L2->Apply(ThreadPrint);
    cout << "Ready list L3 contents:\n";
    readyList_L3->Apply(ThreadPrint);
    /* /modify by liuchinlin @ mp3 */
}
